# Results processor service 📈🛰️

## How to build w/ docker 🐳

Keep in mind that the following files are needed in the **"src/"** folder:
- **.config**: file to store the [Auth0](https://auth0.com) properties

- **fb_private_key.json**: [Firebase messaging](https://firebase.google.com/docs/cloud-messaging) keys

### First layer

```
docker build -f ci/Dockerfile -t paulabeatrizolmedo/results_processor .
```

### Second layer

```
docker build -f ci/Dockerfile.gc -t results_processor .
```

## How to run it:

    docker run --rm -it -v $(pwd):/home/project -w /home/project --net=host results_processor bash

Once inside, go to the *src/*  folder, and run:

    uvicorn main:results_service --reload

Which should start the server.

## API documentation:

- [Redoc](https://results-service-lld32gxwaq-uc.a.run.app/redoc)