#
# Copyright (c) 2022 PAULA B. OLMEDO.
#
# This file is part of RESULTS_PROCESSOR
# (see https://gitlab.com/satellite-forecast/results-processor).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from configparser import ConfigParser
import os
import jwt


def set_up(configuration_file, configuration_name):
    """Sets up configuration for the app"""

    env = os.getenv("ENV", configuration_file)

    if env == configuration_file:
        config = ConfigParser()
        config.read(configuration_file)
        config = config[configuration_name]
    else:
        print("error reading configuration")

    return config


class Settings():
    def __init__(self):
        pass

    def get_weatherapi_key(self):
        api_config = set_up(".config", "WEATHERAPI")
        return api_config["WEATHERAPI_KEY"]


class VerifyToken():
    """Does all the token verification using PyJWT"""

    def __init__(self, token):
        self.token = token
        self.config = set_up(".config", "AUTH0")

        # This gets the JWKS from a given URL and does processing so you can
        # use any of the keys available
        jwks_url = f'https://{self.config["DOMAIN"]}/.well-known/jwks.json'
        self.jwks_client = jwt.PyJWKClient(jwks_url)

    def verify(self):
        # This gets the 'kid' from the passed token
        try:
            self.signing_key = self.jwks_client.get_signing_key_from_jwt(
                self.token
            ).key
        except jwt.exceptions.PyJWKClientError as error:
            return {"status": "error", "msg": error.__str__()}
        except jwt.exceptions.DecodeError as error:
            return {"status": "error", "msg": error.__str__()}

        try:
            payload = jwt.decode(
                self.token,
                self.signing_key,
                algorithms=self.config["ALGORITHMS"],
                audience=self.config["API_AUDIENCE"],
                issuer=self.config["ISSUER"],
            )
        except Exception as e:
            return {"status": "error", "message": str(e)}

        return payload
