#
# Copyright (c) 2022 PAULA B. OLMEDO.
#
# This file is part of RESULTS_PROCESSOR
# (see https://gitlab.com/satellite-forecast/results-processor).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

APP_NAME = 'https://safo-288221-default-rtdb.firebaseio.com/'


class FirebaseManagment:
    def __init__(self):
        pass

    @staticmethod
    def __authenticate_database():
        """
        autentica e inicializa instancia de firebase realtime database
        TODO: manejar errores
        """
        keys = credentials.Certificate('./fb_private_key.json')

        firebase_admin.initialize_app(keys, {
            'databaseURL': APP_NAME
        })

    def set(self, main_node, tag_name, dict_object):
        """
        guarda resultados/lugares
        @main_node : nombre del nodo padre
        @tag_name : nombre del nodo hijo
        @dict_object : nombre del objeto a guardar
        TODO: manejar errores
        """
        try:
            firebase_admin.get_app(firebase_admin._DEFAULT_APP_NAME)
        except ValueError:
            self.__authenticate_database()

        reference = db.reference(main_node)
        results_table = reference.child(tag_name)
        results_table.set(dict_object)

    def get(self, main_node):
        try:
            firebase_admin.get_app(firebase_admin._DEFAULT_APP_NAME)
        except ValueError:
            self.__authenticate_database()

        # por ahora creo que me basta con los padres
        reference = db.reference(main_node)
        return reference.get()

    def get_child(self, main_node, child_node):
        try:
            firebase_admin.get_app(firebase_admin._DEFAULT_APP_NAME)
        except ValueError:
            self.__authenticate_database()

        reference = db.reference(main_node)
        referece_child = reference.child(child_node)
        return referece_child.get()

    def update(self, main_node, tag_name, dict_object):
        """
        actualiza un determinado valor de una determinada tabla
        @main_node : nombre del nodo padre
        @tag_name : nombre del nodo hijo
        @dict_object : objeto a actualizar, ver referencias en: https://firebase.google.com/docs/database/admin/save-data#python_2
        para evitar pisar valores anteriors
        TODO: manejar errores
        """
        try:
            firebase_admin.get_app(firebase_admin._DEFAULT_APP_NAME)
        except ValueError:
            self.__authenticate_database()

        reference = db.reference(main_node)
        child_node = reference.child(tag_name)
        child_node.update(dict_object)

    def set_child(self, table, main_node, child_node, dict_object):
        try:
            firebase_admin.get_app(firebase_admin._DEFAULT_APP_NAME)
        except ValueError:
            self.__authenticate_database()

        reference = db.reference(table)
        uuid_ref = reference.child(main_node)
        child_ref = uuid_ref.child(child_node)
        child_ref.set(dict_object)

    def update_child(self, table, main_node, child_node, dict_object):
        """
        TODO: manejar errores
        """
        try:
            firebase_admin.get_app(firebase_admin._DEFAULT_APP_NAME)
        except ValueError:
            self.__authenticate_database()

        reference = db.reference(table)
        main = reference.child(main_node)
        child = main.child(child_node)
        child.update(dict_object)

    def delete_child(self, table, main_node, child_node):
        """
        TODO: manejar errores
        """
        try:
            firebase_admin.get_app(firebase_admin._DEFAULT_APP_NAME)
        except ValueError:
            self.__authenticate_database()

        reference = db.reference(table)
        main = reference.child(main_node)
        child = main.child(child_node)
        child.delete()
