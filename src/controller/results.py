#
# Copyright (c) 2022 PAULA B. OLMEDO.
#
# This file is part of RESULTS_PROCESSOR
# (see https://gitlab.com/satellite-forecast/results-processor).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from dataclasses import dataclass
from numpy import float64
from controller.places import Places
from controller.weather import WeatherStatus, RangeDict
from firebase_managment.configuration import FirebaseManagment
import geopandas as gpd
from shapely.geometry import Point
from typing import List
from pydantic import BaseModel


@dataclass
class Results:
    status: str
    latitude: float64
    longitude: float64


class ResultsResponse(BaseModel):
    status: str
    latitude: float64
    longitude: float64
    current_alert_status: str
    forecast_alert_status: str


class TaggedPlace(BaseModel):
    name: str
    response: ResultsResponse


class ResultsList(BaseModel):
    __root__: List[TaggedPlace]


class ProcessingResults:
    """
    clase que representa cada una de las resultados procesados
    TODO: ¿harán falta los uuid?
    """

    def __init__(self):
        pass

    def set_results(self, results_path):
        """
        dados los datos de "places" tengo que tagear los resultados

        TODO: revisar manejo de errores
        """
        raw_results = gpd.read_file(results_path)

        places = Places("", -1, -1)
        available_places = places.get_places_wo_list()

        db = FirebaseManagment()

        for tag, value in available_places.items():
            lat = value.get('latitude')
            long = value.get('longitude')

            point = Point(long, lat)
            print(point)
            print(tag)

            for _, row in raw_results.iterrows():
                coordinates = row['geometry']

                if coordinates.contains(point):
                    results = Results(
                        status=row['status'], latitude=lat, longitude=long).__dict__
                    print("to set")
                    print(results)
                    db.set("results", tag_name=tag, dict_object=results)

    def get_results(self, tag=""):
        db = FirebaseManagment()

        if tag == "":
            raw_results = db.get("results")
            # TODO: improve database to handle this by default
            pydantic_results = [
                {'name': k, 'response': raw_results[k]} for k in raw_results]

            return ResultsList(__root__=pydantic_results)

        else:
            raw_results = db.get_child("results", tag)
            if raw_results:
                pydantic_results = TaggedPlace(name=tag, response=raw_results)
                return ResultsList(__root__=[pydantic_results])

            return ResultsList(__root__=[])

    def get_results_wo_list(self):
        db = FirebaseManagment()

        return db.get("results")

    def set_results_alert(self, api_key, type):
        """
            Endpoint que busca los datos del clima de las zonas registradas y actualiza el estado de alerta de cada una de ellas
            type = current / forecast
        """
        raw_status = {
            'healthy': 10.5,
            'barely_healthy': 21,
            'stressed': 31.5,
            'dead': 35
        }

        results_list = self.get_results_wo_list()

        weatherStatus = WeatherStatus()
        db = FirebaseManagment()

        for tag, value in results_list.items():
            lat = value.get('latitude')
            long = value.get('longitude')
            status = value.get('status')

            coordinates = f'{lat},{long}'

            status_value = raw_status[status]

            weather = weatherStatus.get_current_weather(
                coordinates, api_key)

            weather_fire_index = weatherStatus.get_weather_fire_index(
                weather, type)

            fire_index = weather_fire_index + status_value

            fire_alert_ranges = RangeDict(
                {range(0, 24): 'low', range(25, 50): 'moderate',
                 range(50, 75): 'high', range(75, 100): 'extreme'})

            fire_alert = ""
            try:
                fire_alert = fire_alert_ranges[int(fire_index)]
            except KeyError:
                raise

            alert_status = {
                f'{type}_alert_status': fire_alert,
            }

            db.update("results", tag_name=tag, dict_object=alert_status)
