#
# Copyright (c) 2022 PAULA B. OLMEDO.
#
# This file is part of RESULTS_PROCESSOR
# (see https://gitlab.com/satellite-forecast/results-processor).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from dataclasses import dataclass
from numpy import float64
from pydantic import BaseModel
from firebase_managment.configuration import FirebaseManagment
from typing import List
from datetime import date


@dataclass
class Coordinates:
    latitude: float64
    longitude: float64


@dataclass
class PlaceSuggestion:
    tag: str
    date: str
    reason: str
    latitude: float64
    longitude: float64
    status: str
    resolution: str


class CoordinatesResponse(BaseModel):
    latitude: float64
    longitude: float64


class PlacesResponse(BaseModel):
    name: str
    coordinates: CoordinatesResponse


class PlacesList(BaseModel):
    __root__: List[PlacesResponse]


class SuggestedPlacesList(BaseModel):
    __root__: List[PlaceSuggestion]


class Places:
    """
    clase que representa cada una de las "zonas"
    TODO: ¿harán falta los uuid?
    """

    def __init__(self, tag_name, latitude, longitude):
        # self.id = str(uuid.uuid1())
        self.tag_name = tag_name
        self.latitude = latitude
        self.longitude = longitude

    def set_places(self):
        coordinates = Coordinates(self.latitude, self.longitude)

        db = FirebaseManagment()
        db.set("places", tag_name=self.tag_name,
               dict_object=coordinates.__dict__)

    def get_places_wo_list(self, tag=""):
        db = FirebaseManagment()
        if tag == "":
            return db.get("places")
        else:
            return db.get_child("places", tag)

    def get_places(self, tag=""):
        db = FirebaseManagment()
        if tag == "":
            raw_results = db.get("places")
            print(raw_results)
            pydantic_results = [
                {'name': k, 'coordinates': raw_results[k]} for k in raw_results]
            return PlacesList(__root__=pydantic_results)
        else:
            raw_results = db.get_child("places", tag)
            if raw_results:
                pydantic_results = PlacesResponse(
                    name=tag, coordinates=raw_results)
                return PlacesList(__root__=[pydantic_results])
            return PlacesList(__root__=[])

    def set_place_suggestion(self, uuid: str, reason: str):
        suggestion = PlaceSuggestion(tag=self.tag_name, date=str(date.today()), reason=reason,
                                     latitude=self.latitude, longitude=self.longitude, status="Pendiente", resolution="Pendiente")

        db = FirebaseManagment()
        db.set_child(table="places_suggestions", main_node=uuid, child_node=self.tag_name,
                     dict_object=suggestion.__dict__)

    def update_place_suggestion(self, uuid: str, reason: str, status: str, resolution: str):
        suggestion = PlaceSuggestion(tag=self.tag_name, date=str(date.today()), reason=reason,
                                     latitude=self.latitude, longitude=self.longitude, status=status, resolution=resolution)

        db = FirebaseManagment()
        db.update_child(table="places_suggestions", main_node=uuid, child_node=self.tag_name,
                        dict_object=suggestion.__dict__)

    def get_suggested_places(self, uuid=""):
        db = FirebaseManagment()
        # agregarle condición en caso de que necesite devolver todas las sugerencias existentes
        if uuid == "":
            return db.get("places_suggestions")

        return db.get_child("places_suggestions", uuid)

    def deletes_suggested_places(self, uuid: str, tag: str):
        db = FirebaseManagment()
        return db.delete_child("places_suggestions", uuid, tag)
