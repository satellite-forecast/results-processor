#
# Copyright (c) 2022 PAULA B. OLMEDO.
#
# This file is part of RESULTS_PROCESSOR
# (see https://gitlab.com/satellite-forecast/results-processor).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from weatherapi.weatherapi_client import WeatherapiClient
WEATHERAPI_URL = 'http://api.weatherapi.com/v1/current.json'


class RangeDict(dict):
    def __getitem__(self, item):
        if not isinstance(item, range):
            for key in self:
                if item in key:
                    return self[key]
            raise KeyError(f'"{item}": not a valid value')
        else:
            return super().__getitem__(item)


class WeatherStatus:
    def __init__(self):
        pass

    def get_current_weather(self, coordinates, api_key):
        """
            Función que consulta el estado del clima en una determinada localidad
            input -> string que representa la (latitud, longitud)
        """
        client = WeatherapiClient(api_key)
        ap_is_controller = client.ap_is
        current_weather = ap_is_controller.get_forecast_weather(
            coordinates, 2)  # ver si merece una función aparte, porque igual trae el clima actual

        return current_weather

    def get_weather_fire_index(self, weather, type="current"):
        """
            función que dado el parámetro "weather" calcula un índice parcial
            de peligrosidad de incendio
            input -> objeto devuelto por WeatherAPI
        """

        temperature = weather.current.temp_c
        wind_kph = weather.current.wind_kph
        humidity = weather.current.humidity

        if type == "forecast":
            forecastedday = weather.forecast.forecastday[1].day

            temperature = forecastedday.maxtemp_c
            wind_kph = forecastedday.maxwind_kph
            humidity = forecastedday.avghumidity

        temperature_ranges = RangeDict(
            {range(-10, 9): 2.5, range(10, 12): 5,
             range(12, 14): 7.5, range(14, 16): 10.5,
             range(16, 18): 12, range(18, 20): 15.5,
             range(20, 22): 17.5, range(22, 24): 20,
             range(24, 26): 22.5, range(26, 50): 25})

        humidity_ranges = RangeDict(
            {range(80, 100): 2.5, range(75, 80): 5,
             range(70, 75): 7.5, range(65, 70): 10.5,
             range(60, 65): 12.5, range(55, 60): 15,
             range(50, 55): 17.5, range(45, 50): 20,
             range(40, 45): 22.5, range(0, 40): 25})

        wind_ranges = RangeDict(
            {range(-1, 3): 1.5, range(3, 6): 3,
             range(6, 9): 4.5, range(9, 12): 6,
             range(12, 15): 7.5, range(15, 18): 9,
             range(18, 21): 10.5, range(21, 24): 12,
             range(24, 27): 13.5, range(27, 100): 15})

        try:
            temperature_index = temperature_ranges[int(temperature)]
            humidity_index = humidity_ranges[int(humidity)]
            wind_index = wind_ranges[int(wind_kph)]
        except KeyError:
            raise

        fire_index = temperature_index + humidity_index + wind_index

        return fire_index
