fastapi==0.75.1
uvicorn==0.17.6
firebase_admin==5.2.0
geopandas==0.10.2
numpy==1.22.2
pydantic==1.9.0
Shapely==1.8.1.post1
opencv-python-headless==4.6.0.66