#
# Copyright (c) 2022 PAULA B. OLMEDO.
#
# This file is part of RESULTS_PROCESSOR
# (see https://gitlab.com/satellite-forecast/results-processor).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from fastapi import FastAPI, Response, status, Depends, HTTPException
from typing import Optional
from numpy import float64
from pydantic import BaseModel
from controller.places import Places, PlacesList
from controller.results import ProcessingResults, ResultsList
from controller.weather import WeatherStatus
from api_configuration import Settings, VerifyToken
from functools import lru_cache
from fastapi.security import HTTPBearer
from fastapi.middleware.cors import CORSMiddleware


class RawResults(BaseModel):
    results_path: str
    source_image_path: str


class PlaceSuggestionRequest(BaseModel):
    uuid: str
    tag_name: str
    reason: str
    status: str = "Pendiente"
    resolution: str = "Pendiente"
    latitude: float64
    longitude: float64


class PlacesRequest(BaseModel):
    tag_name: str
    latitude: float64
    longitude: float64


class WeatherRequest(BaseModel):
    coordinates: str


class ResultsResponse(BaseModel):
    status: str
    latitude: float64
    longitude: float64
    current_alert: str
    forecast_alert: str


origins = ["*", ]

description = """
Necessary endpoints to help with the managment of the data generated from the [Image Processor](https://gitlab.com/satellite-forecast/image-processor)
"""

tags_metadata = [
    {
        "name": "results service",
        "description": "Operations related to the processed information of the images.",
    },
    {
        "name": "places service",
        "description": "Operations related to the places managment.",
    },
]


results_service = FastAPI(
    title="Results processor",
    description=description,
    license_info={
        "name": "GPL v3",
        "url": "https://www.gnu.org/licenses/gpl-3.0.html",
    },
    contact={
        "name": "Satellite Forecast",
        "email": "info@satelliteforecast.systems",
    },
    version=1.0,
    openapi_tags=tags_metadata)

results_service.add_middleware(CORSMiddleware, allow_origins=origins,
                               allow_credentials=True, allow_methods=["*"], allow_headers=["*"],)

token_auth_scheme = HTTPBearer()


@lru_cache()
def get_settings():
    return Settings()


def verify_token(token, response: Response):
    token_result = VerifyToken(token.credentials).verify()
    if token_result.get("status"):
        response.status_code = status.HTTP_403_FORBIDDEN
        return token_result


@results_service.get("/results", tags=["results service"], response_model=ResultsList)
async def show_results(response: Response, tag: Optional[str] = None, token: str = Depends(token_auth_scheme)):
    """
        Shows the stored processed results:

        - **tag** (*optional*): to show the results of a certain place.
    """

    verify_token(token, response)
    results = ProcessingResults()

    if tag:
        if all(x.isalpha() or x.isspace() for x in tag):
            tagged_result = results.get_results(tag)
            if len(tagged_result.__root__) == 0:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND, detail=f'No such place: {tag}.')

            response.status_code = status.HTTP_200_OK
            return tagged_result

        else:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=f'Invalid tag name: {tag}.')

    all_results = results.get_results()
    if not all_results:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail="No places registered yet.")

    response.status_code = status.HTTP_200_OK
    return all_results


@results_service.post("/results", tags=["results service"])
async def insert_results(rr: RawResults, response: Response, token: str = Depends(token_auth_scheme)):
    """
        Stores the processed results:

        - **results_path**: location of the raw results *file*. Note that it's an absolute path.
        - **source_image_path**: location of the image to be segmentated.
    """

    verify_token(token, response)

    results = ProcessingResults()
    results.set_results(rr.results_path, rr.source_image_path)

    return {"Processing results correctly stored."}


@results_service.patch("/results", tags=["results service"])
async def update_results_with_fire_alert(type: str, response: Response, token: str = Depends(token_auth_scheme)):
    """
       Updates the stored results to generate fire alerts:

       - **type**: alert to be calculated.

            It can be a *current* alert, or a *forecast* alert, for the following day.
    """

    verify_token(token, response)
    settings = Settings()
    key = settings.get_weatherapi_key()

    alert_types = ["current", "forecast"]
    if type not in alert_types:
        response.status_code = status.HTTP_400_BAD_REQUEST
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f'Invalid alert type: {type}')

    results = ProcessingResults()
    results.set_results_alert(key, type)
    return{"Fire alert correctly updated in Results"}


@results_service.get("/places", tags=["places service"], response_model=PlacesList)
async def show_places(response: Response, tag: Optional[str] = None, token: str = Depends(token_auth_scheme)):
    """
       Shows all the stored places and it's coordinates.

        - **tag** (*optional*): to show a certain place.
    """

    verify_token(token, response)
    place = Places("", -1, -1)

    if tag:
        if all(x.isalpha() or x.isspace() for x in tag):
            tagged_place = place.get_places(tag)
            if len(tagged_place.__root__) == 0:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND, detail=f'No such place: {tag}.')

            response.status_code = status.HTTP_200_OK
            return tagged_place
        else:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=f'Invalid tag name: {tag}.')

    all_places = place.get_places()
    if not all_places:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail="No places registered yet.")

    response.status_code = status.HTTP_200_OK
    return all_places


@results_service.post("/places", tags=["places service"])
async def insert_places(response: Response, pr: PlacesRequest, token: str = Depends(token_auth_scheme)):
    """
        Register a new place in the database.
    """
    verify_token(token, response)

    place = Places(pr.tag_name, pr.latitude, pr.longitude)
    place.set_places()

    return {place}


@results_service.get("/weather", tags=["places service"])
async def show_weather(coordinates: str):
    """
        Shows the current weather of a certain place

        - **coordinates**: place location, written in WGS84 decimal
    """
    settings = Settings()
    key = settings.get_weatherapi_key()

    weather = WeatherStatus()
    w = weather.get_current_weather(coordinates, key)
    return{"weather": w}


@results_service.post("/places/suggestions", tags=["places service"])
async def insert_place_suggestion(response: Response, ps: PlaceSuggestionRequest, token: str = Depends(token_auth_scheme)):
    """
        Adds a new suggested place.
    """

    verify_token(token, response)

    place = Places(ps.tag_name, ps.latitude, ps.longitude)
    place.set_place_suggestion(ps.uuid, ps.reason)

    return {place}


@results_service.patch("/places/suggestions", tags=["places service"])
async def update_place_suggestion(response: Response, ps: PlaceSuggestionRequest, token: str = Depends(token_auth_scheme)):
    """
        Updates a suggested place.
    """

    verify_token(token, response)

    place = Places(ps.tag_name, ps.latitude, ps.longitude)
    place.update_place_suggestion(ps.uuid, ps.reason, ps.status, ps.resolution)

    return {place}


@results_service.get("/places/suggestions", tags=["places service"])
async def get_place_suggestion(response: Response, uuid: Optional[str] = None, token: str = Depends(token_auth_scheme)):
    """
        Gets sugggested places, filtered user or the entire collection (for admin purposes only).
    """

    verify_token(token, response)

    place = Places("", -1, -1)
    if uuid:
        suggestions = place.get_suggested_places(uuid)
    else:
        suggestions = place.get_suggested_places()

    if not suggestions:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail="No suggestions registered yet.")

    response.status_code = status.HTTP_200_OK
    return suggestions


@results_service.delete("/places/suggestions", tags=["places service"])
async def delete_place_suggestion(response: Response, uuid: str, tag: str, token: str = Depends(token_auth_scheme)):
    """
        Deletes suggested place.
    """

    verify_token(token, response)

    place = Places("", -1, -1)
    place.deletes_suggested_places(uuid, tag)

    return {"correctly deleted"}
